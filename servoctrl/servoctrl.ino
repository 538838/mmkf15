#include <Servo.h>
//Servo library code: https://code.google.com/p/arduino/source/browse/trunk/libraries/Servo/?r=1088

byte byteRead;
bool byteHigh;
Servo servos[6];
int servo;
int usec;

void setup() {                
// Turn the Serial Protocol ON
  Serial.begin(115200);
  
  for(int i=0; i < 6;i++){
    servos[i].attach(i+2); // Attach servo to port i
  }
  pinMode(13, OUTPUT); // LED
  byteRead = 0;
  servo = 0;
  byteHigh = true;
  usec = 1500;
}

//Send angle as microseconds, in int16 to increase resolution

void loop() {
   /*  check if data has been sent from the computer: */
  if (Serial.available()) {
    byteRead = Serial.read();
    if(byteHigh){
      usec = (byteRead&B00001111)<<8;
      servo = (byteRead&B11110000)>>4;
      byteHigh = false;
      digitalWrite(13, !digitalRead(13));
    } else{
      usec = usec | byteRead;
      byteHigh = true;
      servos[servo].writeMicroseconds(usec);
      digitalWrite(13, !digitalRead(13));

      //Debug code
      /*
      for(int i = 3; i >= 0; i--){
        if((servo>>i)&0b00000001 == 0b00000001){
          Serial.write('1');
        } else{
          Serial.write('0');
        }
      }
      for(int i = 3; i >= 0; i--){
        if((usec>>i+8)&0b00000001 == 0b00000001){
          Serial.write('1');
        } else{
          Serial.write('0');
        }
      }
      for(int i = 7; i >= 0; i--){
        if((usec>>i)&0b00000001 == 0b00000001){
          Serial.write('1');
        } else{
          Serial.write('0');
        }
      }
      */
      
    }
  }
}
