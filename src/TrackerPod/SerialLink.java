package TrackerPod;

import java.util.concurrent.Semaphore;
import java.util.stream.IntStream;
import jssc.*;

public class SerialLink extends Thread implements SerialPortEventListener {
	//Serial port and settings for serial port
	private SerialPort serialPort;
	private final int BAUDRATE = SerialPort.BAUDRATE_115200;
	private final int DATABITS = SerialPort.DATABITS_8;
	private final int STOPBITS = SerialPort.STOPBITS_1;
	private final int PARITY = SerialPort.PARITY_NONE;
	
	// The limits for the individual motors.
	// uS per degree, mid, max and min position 
	private double[] usPerDegree = new double[]{9.1,-12.1,9.2,-8.5,12.1,-8.7};
	private double[] usMidPosition = new double[]{1300,1500,1300,1300,1500,1300};
	private int[] usMax = new int[]{1800,2000,2000,1600,2200,1600};
	private int[] usMin = new int[]{900,750,950,550,1050,500};
	
	// To only send data when there is new data.
	private Semaphore newData = new Semaphore(1);
	// microseconds to send
	private int[] uS;
	
	/**
	 * 
	 * @return a list with the available serial ports
	 */
	public static String[] getPortList(){
		return SerialPortList.getPortNames();
	}
	
	/**
	 * Creates a serial port with physical port port and initializes it
	 * @param port name of physical port
	 */
	public SerialLink(String port){
		//Start at mid positions
		uS = new int[]{(int)usMidPosition[0],(int)usMidPosition[1],(int)usMidPosition[2],(int)usMidPosition[3],(int)usMidPosition[4],(int)usMidPosition[5]};
		//Create and initialize new serial port
		serialPort = new SerialPort(port);
		try {
			serialPort.openPort();
			serialPort.setParams(BAUDRATE, DATABITS, STOPBITS, PARITY);
			int mask = SerialPort.MASK_RXCHAR;//Prepare mask
            serialPort.setEventsMask(mask);
            // Add event listener if needed
            //serialPort.addEventListener(this);
            //Wait for serial port to be ready
            Thread.sleep(2000);
		} catch (SerialPortException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Tries to set the servos to given angles
	 * @param angle angles in radians
	 * @return true if the position is reachable, else false
	 */
	public boolean setAngle(float[] angle){
		int us[] = new int[]{0,0,0,0,0,0};
		boolean isNan = false;
		for(int i = 0; i < angle.length; i++){
			if(Float.isNaN(angle[i])){
				isNan = true;
				i = angle.length-1;
			}
			us[i] = (int)(angle[i]*180.0/Math.PI*usPerDegree[i]+usMidPosition[i]);
			//Limit the output
			if(us[i] > usMax[i]){
				us[i] = usMax[i];
			} else if(us[i] < usMin[i]){
				us[i] = usMin[i];
			}
		}
		
		// If one or more of the angles is NaN the position is not reachable
		if(!isNan){
			uS = us;
			if(newData.availablePermits() == 0){
				newData.release();
			}
			return true;
		} else{
			return false;
		}
	}
	
	public void run(){
		while(true){
			try {
				senduS(uS);
			} catch (InterruptedException e) {
				
			}
		}
	}
	
	/**
	 * Sends the values of microseconds via the serial port to the arduino
	 * @param uS microseconds to each servo (0-5)
	 * @throws InterruptedException
	 */
	private void senduS(int[] uS) throws InterruptedException {
		newData.acquire();
		//Create servo index
		int[] servo = IntStream.rangeClosed(0, uS.length-1).toArray();
		for(int i = 0; i < uS.length;i++) {
			/* 16 bits divided in 2 bytes, with the highest 4 bits representing 
			 * the servo index and the remaining 12 bits representing the microsecond 
			 * value
			 */  
			byte servoNumber = (byte)(servo[i]<<4);
			byte high = (byte)((uS[i]>>8)&0b00001111);
			byte low = (byte)(uS[i] & 0xff);
			
			try {
				serialPort.writeByte((byte)(servoNumber | high));
				serialPort.writeByte(low);
				//Sleep a short while to make the two bad servos behave a little bit better
				Thread.sleep(50);
			} catch (SerialPortException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		}
	}

	@Override
	/**
	 * Listener for serial events.
	 * Prints the received bytes as chars
	 */
	public void serialEvent(SerialPortEvent event) {
		if (event.isRXCHAR()) {
			try {
				byte buffer[] = serialPort.readBytes(event.getEventValue());
				System.out.println(buffer.length);
				for(int i = 0; i < buffer.length; i++){
					System.out.print((char)buffer[i]);
				}
				System.out.print("\n");
			} catch (SerialPortException e) {
				e.printStackTrace();
			}
        }
	}
	
}
