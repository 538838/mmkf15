package TrackerPod;

import CV.*;
import GUI.*;
import Model.*;
import Control.*;

import java.util.*;
import java.awt.Point;
import org.opencv.core.*;

public class Pod extends Thread{
	// All modules needed for the pod
	private Platform model;
	private CV cv;
	private Control control;
	private Settings guiSettings;
	private mainCamera guiMain;
	private calibCamera guiCalib;
	private SerialLink link;
	
	//Needed variables
	private PVector translation,rotation;
	private Mat rawImage;
	private Point laser,ball;
	private long startTime;
	
	// Default HSV threshold values for laser and ball
	private int[] laserThreshold = {165, 171, 60, 256, 150, 256};
	private int[] ballThreshold = {3, 13, 147, 256, 84, 256};
	
	// Set values
	private final int DEFAULTCAMERA = 1;
	private final int WIDTH = 640; 
	private final int HEIGHT = 480;
	
	public Pod(String serialport, int camera){
		// Select camera, default if none is given
		if(camera == -1){
			cv = new CV(DEFAULTCAMERA,WIDTH,HEIGHT);
		} else {
			cv = new CV(camera,WIDTH,HEIGHT);
		}
		
		// Select serialport, default if none is given
		if(serialport == null){
			link = new SerialLink(SerialLink.getPortList()[0]);
		} else {
			link = new SerialLink(serialport);
		}
		link.start();
		
		model = new Platform();
		control = new Control(WIDTH,HEIGHT);
		
		guiSettings = new Settings(laserThreshold,ballThreshold,this);
		guiMain = new mainCamera(WIDTH, HEIGHT);
		guiCalib = new calibCamera(WIDTH, HEIGHT);
		
		rotation = new PVector(0, 0, 0);
		translation = new PVector(0, 0, 0);
		rawImage = new Mat();
		laser = new Point(0,0);
		ball = new Point(0,0);
		startTime = System.currentTimeMillis();
	}
	
	/*
	 * Runs the tracking constantly
	 */
	public void run(){
		while(true){
			this.track();
		}
	}
	
	/**
	 * Closes program
	 */
	public void close(){
		cv.close();
		System.exit(0);
	}
	
	/*
	 * Main tracking function that calls functions for
	 * image retrieving, computer vision, control, seriallink to robot
	 * and a gui that controls the parameters
	 */
	private void track(){
		// Calculates execution frequency and displays it on the GUI
		guiSettings.setFrequency(1000.0/(System.currentTimeMillis()-startTime));
		startTime = System.currentTimeMillis();
		
		// Fetch image and filtering values
		rawImage = cv.fetchImage();
		laserThreshold = guiSettings.getLaserThreshold();
		ballThreshold = guiSettings.getBallThreshold();
		
		// If you want to update the GUI with the filtered hue/sat/value images, enable this function
		// If not active, the algorithm executes faster
		if(guiSettings.getSource() != 0){
			// Shows the filtered images if the search target is the laser
			if(guiSettings.getSource() == 1) {
				cv.findPositionSimple(ball, ballThreshold, true, false);
				List<Mat> res = cv.findPositionSimple(laser, laserThreshold, false, true);
				guiCalib.update(res);
			}
			// Shows the filtered images if the search target is the ball
			else if(guiSettings.getSource() == 2) { 
				cv.findPositionSimple(laser, laserThreshold, true, false);
				List<Mat> res = cv.findPositionSimple(ball, ballThreshold, false, true);
				guiCalib.update(res);
			}
		} 
		// No updating of the filtered images occurs here, for faster execution 
		else {
			cv.findPositionSimple(laser, laserThreshold, true, false);
			cv.findPositionSimple(ball, ballThreshold, false, false);
		}
		// Updates the main window 
		guiMain.update(rawImage, ball, laser);
		
		// Manual control of the platform is done here, from the sliders in the GUI
		if(guiSettings.getControl() == 0){
			rotation = guiSettings.getRotation();
			translation = guiSettings.getTranslation();
			model.applyRotationAndTranslation(rotation, translation);
			float[] servoAngles = model.getAlpha();
			link.setAngle(servoAngles);
		} 
		// Automatic control of the platform is done here, tracking ball and laser
		else if(guiSettings.getControl() != 0) {
			if(laser.x != -1 && ball.x != -1) {
				control.setParam(guiSettings.getP());
				// control adds the difference angles to the current angle
				// Always "integrating" the error
				PVector cP = control.rotAngles(laser, ball);
				rotation.add(cP);
				model.applyRotationAndTranslation(rotation, translation);
				float[] servoAngles = model.getAlpha();
				// If the angles can not be set, the maximum position has been reached
				// So we subtract the angle again to not strain servos - anti-windup
				if(link.setAngle(servoAngles) == false){
					rotation.sub(cP);
				}
			}
		}
	}
}
