package Model;

// Rotation or translation vector class for storing x, y and z values and functions for some vector calculations
public class PVector{
	
	public float x,y,z;

	public PVector(float x, float y, float z){
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public PVector(){
		super();
	}

	public void set(PVector pv){
		this.x = pv.x;
		this.y = pv.y;
		this.z = pv.z;
	}
	public PVector add(PVector pv){
		this.x = this.x + pv.x;
		this.y = this.y + pv.y;
		this.z = this.z + pv.z;
		return new PVector(x,y,z);
	}
	
	public static PVector add(PVector pv1,PVector pv2){
		float x = pv1.x + pv2.x;
		float y = pv1.y + pv2.y;
		float z = pv1.z + pv2.z;
		return new PVector(x,y,z);
 	}

 	public static PVector sub(PVector pv1, PVector pv2){
 		float x = pv1.x - pv2.x;
		float y = pv1.y - pv2.y;
		float z = pv1.z - pv2.z;
		return new PVector(x,y,z);
 	}
 	
	public PVector sub(PVector pv){
		this.x = this.x - pv.x;
		this.y = this.y - pv.y;
		this.z = this.z - pv.z;
		return new PVector(x,y,z);
	}

	public float magSq(){
		float result;
		result = x * x + y * y + z * z;
		return result;
	}
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("x: "+x+" y: "+ y+" z: "+z);
		return sb.toString();
	}

}