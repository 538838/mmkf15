package Model;

public class Platform{
	private PVector translation, rotation, initialHeight;
	private PVector[] baseJoint, platformJoint, q, l, A;
	private float[] alpha;
	private final float PI = (float) Math.PI;

  	private final float baseAngles[] = {0, PI/6, 4*PI/6, 5*PI/6, 8*PI/6, 9*PI/6}; 	
  	private final float shift = 69.75f;

  	private final float platformAngles[] = {(float)(Math.toRadians(shift + 250.5)), (float)Math.toRadians(shift), 
  		(float)Math.toRadians(shift + 10.5), (float)Math.toRadians(shift + 120), (float)Math.toRadians(shift + 130.5), (float)Math.toRadians(shift + 240)};
	// Relative angles from the servos to the x-axis
	private final float betaShift = 0;
	private final float beta[] = {
		betaShift+0, PI/6+betaShift, 4*PI/6+betaShift, 5*PI/6+betaShift, 8*PI/6+betaShift, 9*PI/6+betaShift};
	
	// Measurements of the platform
	private final float INITIAL_HEIGHT = 100;
	private final float BASE_RADIUS = 150;
	private final float PLATFORM_RADIUS = 132;
	private final float SERVO_LENGTH = 17;
	private final float LEG_LENGTH = 147;

	public Platform(){
		// Initialize variables
		translation = new PVector();
	    rotation = new PVector();
	    baseJoint = new PVector[6];
	    platformJoint = new PVector[6];
	    initialHeight = new PVector(0, 0, INITIAL_HEIGHT);
	    alpha = new float[6];
	    q = new PVector[6];
	    l = new PVector[6];
	    A = new PVector[6];

	    // Set base joint positions
	    for(int i = 0; i<6; i++){
	    	float mx = BASE_RADIUS * (float)Math.cos( baseAngles[i] );
      		float my = BASE_RADIUS * (float) Math.sin( baseAngles[i] );
      		baseJoint[i] = new PVector(mx, my, 0);
	    }

	    // Set the platform joint positions
	    for (int i=0; i<6; i++) {
	     	float mx = PLATFORM_RADIUS * (float)Math.cos(platformAngles[i]);
	    	float my = PLATFORM_RADIUS * (float)Math.sin(platformAngles[i]);
	    	platformJoint[i] = new PVector(mx, my, 0);
    	}
    	// Set q[i], l[i] ,a[i] to PVector(0,0,0) 
    	for(int i = 0; i<6; i ++){
    		q[i] = new PVector(0, 0, 0);
	    	l[i] = new PVector(0, 0, 0);
			A[i] = new PVector(0, 0, 0);
    	}
    	calcQ();
    }
    
	/**
	 * Applies rotation and translation and calculates servo angles 
	 * @param rotation rotation to apply
	 * @param translation translation to apply
	 */
    public void applyRotationAndTranslation(PVector rotation, PVector translation){
    	this.rotation.set(rotation);
    	this.translation.set(translation);
    	calcQ();
    	calcAlpha();
    }
    
    /*
     * Calculate the Q-vector 
     */
    private void calcQ() {
    	for (int i=0; i<6; i++) {
	      // Rotation
	    	q[i].x = (float) (Math.cos( rotation.z ) * Math.cos(rotation.y ) * platformJoint[i].x +
	    	  (-Math.sin(rotation.z) * Math.cos(rotation.x ) + Math.cos(rotation.z) * Math.sin(rotation.y) * Math.sin(rotation.x)) * platformJoint[i].y +
	    	  (Math.sin(rotation.z) * Math.sin(rotation.x) + Math.cos(rotation.z) * Math.sin(rotation.y) * Math.cos(rotation.x)) * platformJoint[i].z);

	    	q[i].y =(float)(Math.sin(rotation.z) * Math.cos(rotation.y) * platformJoint[i].x +
	    	  (Math.cos(rotation.z) * Math.cos(rotation.x) + Math.sin(rotation.z) * Math.sin(rotation.y) * Math.sin(rotation.x)) * platformJoint[i].y +
	    	  (-Math.cos(rotation.z)* Math.sin(rotation.x)+ Math.sin(rotation.z) * Math.sin(rotation.y) * Math.cos(rotation.x)) * platformJoint[i].z);

	    	q[i].z =(float) (-Math.sin(rotation.y) * platformJoint[i].x +
	    	  Math.cos(rotation.y) * Math.sin(rotation.x) * platformJoint[i].y +
	    	  Math.cos(rotation.y) * Math.cos(rotation.x)*platformJoint[i].z);
	    	// Translation
	    	q[i].add(PVector.add(translation, initialHeight));
	    	l[i] = PVector.sub(q[i], baseJoint[i] );
	    }
	}
    
    /*
     * Calculate the alpha vector 
     */
	private void calcAlpha(){
		for (int i=0; i<6; i++) {
			float L = l[i].magSq()  - (LEG_LENGTH * LEG_LENGTH) + (SERVO_LENGTH * SERVO_LENGTH);
	      	float M = 2 * SERVO_LENGTH * (q[i].z - baseJoint[i].z);	      	
	      	float N = 2 * SERVO_LENGTH * ( (float)Math.cos(beta[i]) * (q[i].x - baseJoint[i].x) + (float)Math.sin(beta[i]) * (q[i].y-baseJoint[i].y));
	      	float alphaRadians = (float)Math.asin(L / (float)Math.sqrt(M * M + N * N)) - (float)Math.atan2(N, M);
	      	alpha[i] = alphaRadians;
   		}
	}
	
	/**
	 * Returns the servo angles needed for the applied translation and rotation
	 * @return servo angles in an float vector
	 */
	public float[] getAlpha(){
		return alpha;
	}
}	