package GUI;

import java.awt.Point;
import javax.swing.*;
import org.opencv.core.*;
import org.opencv.imgproc.*;

public class mainCamera extends JFrame{
	private static final long serialVersionUID = 1L;
	private JLabel video;
	//Radius of the circles that mark laser and ball positions
	private final int RADIUS = 3;
	
	/**
	 * Creates a new window for displaying the raw camera image, with laser and ball
	 * positions marked.
	 * @param width
	 * @param height
	 */
	public mainCamera(int width, int height){
		this.setTitle("Main image");
		video = new JLabel();
		video.setSize(width, height);
		video.setIcon(new ImageIcon(GuiUtils.Mat2BufferedImage(new Mat(height,width,CvType.CV_8UC3))));
		this.add(video);
		this.pack();
		this.setVisible(true);
	}
	
	/**
	 * Updates the displayed image with marked ball and laser positions
	 * @param source raw image to display
	 * @param ball ball position
	 * @param laser laser position
	 */
	public void update(Mat source, Point ball,Point laser){
		Imgproc.circle(source, new org.opencv.core.Point(laser.x,laser.y), RADIUS, new Scalar(0, 0, 255), -1);
		Imgproc.circle(source, new org.opencv.core.Point(ball.x,ball.y), RADIUS, new Scalar(0, 255, 0), -1);
		video.setIcon(new ImageIcon(GuiUtils.Mat2BufferedImage(source)));
	}
}
