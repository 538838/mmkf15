package GUI;

import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import Model.PVector;
import TrackerPod.Pod;

public class Settings extends JFrame implements ActionListener{
	private static final long serialVersionUID = 1L;
	// Calling pod, to be able to close program
	private Pod parent;
	
	// Int arrays to be able to change value by reference 
	private int[] HueLaserLow, HueLaserHigh;
	private int[] SatLaserLow, SatLaserHigh;
	private int[] ValLaserLow, ValLaserHigh;
	private int[] HueBallLow, HueBallHigh;
	private int[] SatBallLow, SatBallHigh;
	private int[] ValBallLow, ValBallHigh;
	private int[] transX, transY, transZ;
	private int[] rotX, rotY, rotZ;
	private int[] Pvalue;
	
	//Base values for translation(mm) and rotation(rad) sliders, which are adjustable between -100 and 100
	// somewhat arbitrary chosen
	private final float TRANSBASE = 0.1f;
	private final float ROTBASE = 0.0005f;
	//Base value for the P-value for control slider, which is adjustable between 0 and 100
	// chosen to make the 50*PBASE = default P-value
	private final float PBASE = 0.001f;
	
	// JLabel for showing program frequency
	private JLabel frequencyLabel;
	
	// Values changed by the radio buttons 
	private int calibSource; 
	private int controlSource;
	public static int MANUAL = 0;
	public static int AUTOMATIC = 1;
	public static int OFF = 0;
	public static int LASER = 1;
	public static int BALL = 2;
	
	/**
	 * Creates a window with settings for the trackerpod
	 * @param defaultLaser starting values of the HSV-thresholds 
	 * @param defaultBall starting values of the HSV-thresholds
	 */
	public Settings(int[] defaultLaser,int[] defaultBall,Pod parent){
		this.parent = parent;
		this.setTitle("Settings");
		this.setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
		
		//=== Display frequency of the control algorithm ===
		// and three radio buttons for the calibration window source 
		JPanel view = new JPanel();
		view.setLayout(new BoxLayout(view, BoxLayout.X_AXIS));
		frequencyLabel = new JLabel("Frequency: 0 Hz");
		calibSource = 0; // Start with calibration window
		JRadioButton ballButton = new JRadioButton("Ball");
		ballButton.setActionCommand("Rb");
		ballButton.addActionListener(this);
		JRadioButton laserButton = new JRadioButton("Laser");
		laserButton.setActionCommand("Rl");
		laserButton.addActionListener(this);
		JRadioButton offButton = new JRadioButton("Off");
		offButton.setActionCommand("Ro");
		offButton.addActionListener(this);
		offButton.setSelected(true);
		ButtonGroup calibration = new ButtonGroup();
		calibration.add(ballButton);
		calibration.add(laserButton);
		calibration.add(offButton);
		view.add(frequencyLabel);
		view.add(ballButton);
		view.add(laserButton);
		view.add(offButton);
		
		//=== Display sliders for HSV threshold for the laser ===
		// initialize with defaultLaser values 
		HueLaserLow = new int[]{defaultLaser[0]};
		HueLaserHigh = new int[]{defaultLaser[1]};
		SatLaserLow = new int[]{defaultLaser[2]};
		SatLaserHigh = new int[]{defaultLaser[3]};
		ValLaserLow = new int[]{defaultLaser[4]};
		ValLaserHigh = new int[]{defaultLaser[5]};
		// Create sliders and labels for hue, saturation and value
		JPanel HueLaser = new JPanel();
		HueLaser.setLayout(new BoxLayout(HueLaser, BoxLayout.X_AXIS));
		HueLaser.add(new JLabel("Laser Hue "));
		HueLaser.add(new Slider(0,256,HueLaserLow));
		HueLaser.add(new Slider(0,256,HueLaserHigh));
		JPanel SatLaser = new JPanel();
		SatLaser.setLayout(new BoxLayout(SatLaser, BoxLayout.X_AXIS));
		SatLaser.add(new JLabel("Laser Sat "));
		SatLaser.add(new Slider(0,256,SatLaserLow));
		SatLaser.add(new Slider(0,256,SatLaserHigh));
		JPanel ValLaser = new JPanel();
		ValLaser.setLayout(new BoxLayout(ValLaser, BoxLayout.X_AXIS));
		ValLaser.add(new JLabel("Laser Val "));
		ValLaser.add(new Slider(0,256,ValLaserLow));
		ValLaser.add(new Slider(0,256,ValLaserHigh));
		
		//=== Display sliders for HSV threshold for the laser ===
		// initialize with defaultBall values
		HueBallLow = new int[]{defaultBall[0]};
		HueBallHigh = new int[]{defaultBall[1]};
		SatBallLow = new int[]{defaultBall[2]};
		SatBallHigh = new int[]{defaultBall[3]};
		ValBallLow = new int[]{defaultBall[4]};
		ValBallHigh = new int[]{defaultBall[5]};
		// Create sliders and labels for hue, saturation and value
		JPanel hueBall = new JPanel();
		hueBall.setLayout(new BoxLayout(hueBall, BoxLayout.X_AXIS));
		hueBall.add(new JLabel("Ball Hue "));
		hueBall.add(new Slider(0,256,HueBallLow));
		hueBall.add(new Slider(0,256,HueBallHigh));
		JPanel satBall = new JPanel();
		satBall.setLayout(new BoxLayout(satBall, BoxLayout.X_AXIS));
		satBall.add(new JLabel("Ball Sat "));
		satBall.add(new Slider(0,256,SatBallLow));
		satBall.add(new Slider(0,256,SatBallHigh));
		JPanel valBall = new JPanel();
		valBall.setLayout(new BoxLayout(valBall, BoxLayout.X_AXIS));
		valBall.add(new JLabel("Ball Val "));
		valBall.add(new Slider(0,256,ValBallLow));
		valBall.add(new Slider(0,256,ValBallHigh));
		
		//=== Display radio buttons for control source (Computer vision, Manual) ==
		// start with manual control
		controlSource = 0;
		JPanel control = new JPanel();
		control.setLayout(new BoxLayout(control, BoxLayout.X_AXIS));
		JRadioButton autoButton = new JRadioButton("Auto");
		autoButton.setActionCommand("Ra");
		autoButton.addActionListener(this);
		JRadioButton manualButton = new JRadioButton("Manual");
		manualButton.setActionCommand("Rm");
		manualButton.addActionListener(this);
		manualButton.setSelected(true);
		ButtonGroup controlGroup = new ButtonGroup();
		controlGroup.add(autoButton);
		controlGroup.add(manualButton);
		control.add(autoButton);
		control.add(manualButton);
		
		//=== Display slider for P-value for the controller ===
		// start at the midpoint of the slider
		Pvalue = new int[]{50};
		JPanel controlValues = new JPanel();
		controlValues.setLayout(new BoxLayout(controlValues, BoxLayout.X_AXIS));
		controlValues.add(new JLabel("P-value "));
		controlValues.add(new Slider(0,100,Pvalue));
		
		//=== Display sliders for X,Y,Z translation and rotation for manual control ===
		// start with no translation and no rotation
		transX = new int[]{0};
		transY = new int[]{0};
		transZ = new int[]{0};
		rotX = new int[]{0};
		rotY = new int[]{0};
		rotZ = new int[]{0};
		// One row per axis 
		JPanel xPose = new JPanel();
		xPose.setLayout(new BoxLayout(xPose, BoxLayout.X_AXIS));
		xPose.add(new JLabel("X trans/rot "));
		xPose.add(new Slider(-100,100,transX));
		xPose.add(new Slider(-100,100,rotX));
		JPanel yPose = new JPanel();
		yPose.setLayout(new BoxLayout(yPose, BoxLayout.X_AXIS));
		yPose.add(new JLabel("Y trans/rot "));
		yPose.add(new Slider(-100,100,transY));
		yPose.add(new Slider(-100,100,rotY));
		JPanel zPose = new JPanel();
		zPose.setLayout(new BoxLayout(zPose, BoxLayout.X_AXIS));
		zPose.add(new JLabel("Z trans/rot "));
		zPose.add(new Slider(-100,100,transZ));
		zPose.add(new Slider(-100,100,rotZ));
		
		//=== Assemble the blocks into one window, with some space
		this.add(view);
		this.add(Box.createRigidArea(new Dimension(0,15)));
		this.add(HueLaser);
		this.add(SatLaser);
		this.add(ValLaser);
		this.add(Box.createRigidArea(new Dimension(0,10)));
		this.add(hueBall);
		this.add(satBall);
		this.add(valBall);
		this.add(Box.createRigidArea(new Dimension(0,15)));
		this.add(control);
		this.add(controlValues);
		this.add(Box.createRigidArea(new Dimension(0,15)));
		this.add(xPose);
		this.add(yPose);
		this.add(zPose);
		
		// Add windowlistener
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.addWindowListener(new wAdapter());
		
		// Pack window
		this.pack();
		this.setVisible(true);
	}
	
	/**
	 * Called when a radio button is selected.  
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		switch(e.getActionCommand()){
		case "Rb": 	calibSource = BALL;
					break;
		case "Rl": 	calibSource = LASER;
					break;
		case "Ro": 	calibSource = OFF;
					break;
		case "Ra": 	controlSource = AUTOMATIC;
					break;
		case "Rm": 	controlSource = MANUAL;
					break;
		}
		
	}
	
	/**
	 * Updates the window frequency label with new frequency
	 * @param frequency
	 */
	public void setFrequency(double frequency){
		frequencyLabel.setText(frequencyLabel.getText().split(":")[0] + ": " + (int)frequency + " Hz");
	}
	
	/**
	 * @return selected source for the calibration window
	 */
	public int getSource(){
		return calibSource;
	}
	
	/**
	 * @return selected source for the control
	 */
	public int getControl(){
		return controlSource;
	}
	
	/**
	 * @return the P-value for the control
	 */
	public float getP(){
		return Pvalue[0]*PBASE;
	}
	
	/**
	 * @return manual control translation values
	 */
	public PVector getTranslation(){
		return new PVector(transX[0]*TRANSBASE,transY[0]*TRANSBASE,transZ[0]*TRANSBASE);
	}
	
	/**
	 * @return manual control rotation values
	 */
	public PVector getRotation(){
		return new PVector(rotX[0]*ROTBASE,rotY[0]*ROTBASE,rotZ[0]*ROTBASE);
	}
	
	/** 
	 * @return laser threshold values for automatic (computer vision) control
	 */
	public int[] getLaserThreshold(){
		return new int[]{HueLaserLow[0],HueLaserHigh[0],SatLaserLow[0],SatLaserHigh[0],ValLaserLow[0],ValLaserHigh[0]};
	}
	
	/** 
	 * @return ball threshold values for automatic (computer vision) control
	 */
	public int[] getBallThreshold(){
		return new int[]{HueBallLow[0],HueBallHigh[0],SatBallLow[0],SatBallHigh[0],ValBallLow[0],ValBallHigh[0]};
	}
	
	/*
	 * Class for catching windowClosing event.
	 * To make sure the current window is closed and program exits
	 */
    private class wAdapter extends WindowAdapter{
    	@Override
        public void windowClosing(WindowEvent event) throws RuntimeException {
    		((Window) event.getSource()).dispose();
    		parent.close();
        }
    }
	
	/*
	 * A JSlider with current value displayed after the slider
	 *  and adds a listener that changes the first value of the given int array
	 *  when the slider changes
	 */
	private class Slider extends JPanel implements ChangeListener {
		private static final long serialVersionUID = 1L;
		private int[] value;
		private JLabel val;
		
		private Slider(int min, int max,int[] value){
			super();
			this.value = value;
			this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
			val = new JLabel("" + value[0]);
			JSlider slider = new JSlider(min,max,value[0]);
			slider.addChangeListener(this);
			this.add(slider);
			this.add(val);
		}
		
		@Override
		public void stateChanged(ChangeEvent e) {
			JSlider source = (JSlider)e.getSource();
			value[0] = source.getValue();
			val.setText("" + value[0]);
		}
	}	
}
