package GUI;

import java.awt.image.BufferedImage;
import java.util.List;
import javax.swing.*;
import org.opencv.core.*;

public class calibCamera extends JFrame{
	private static final long serialVersionUID = 1L;

	private JLabel sum,hue,sat,val;
	private int width;
	private int height;
	
	/**
	 * Creates new calibration window with areas for summary image and the filtered three channels (HSV)
	 * @param width width of the input images
	 * @param height height of the input images
	 */
	public calibCamera(int width, int height){
		this.setTitle("Calibration image");
		setLayout(new BoxLayout(getContentPane(), BoxLayout.X_AXIS));
		this.width = width;
		this.height = height;
		// Create area for total filtration image
		sum = new JLabel();
		sum.setSize(width, height);
		sum.setIcon(new ImageIcon(GuiUtils.Mat2BufferedImage(new Mat(height,width,CvType.CV_8UC1))));
		
		// Create areas for filtered Hue, Saturation and Value images
		JPanel hsv = new JPanel();
		hsv.setLayout(new BoxLayout(hsv, BoxLayout.Y_AXIS));
		hue = new JLabel();
		sat = new JLabel();
		val = new JLabel();
		hue.setSize(width/2,height/2);
		sat.setSize(width/2,height/2);
		val.setSize(width/2,height/2);
		BufferedImage h = GuiUtils.Mat2BufferedImage(new Mat(height,width,CvType.CV_8SC1));
		BufferedImage s = GuiUtils.Mat2BufferedImage(new Mat(height,width,CvType.CV_8SC1));
		BufferedImage v = GuiUtils.Mat2BufferedImage(new Mat(height,width,CvType.CV_8SC1));
		hue.setIcon(new ImageIcon(h.getScaledInstance(height/2, -1, BufferedImage.SCALE_DEFAULT)));
		sat.setIcon(new ImageIcon(s.getScaledInstance(height/2, -1, BufferedImage.SCALE_DEFAULT)));
		val.setIcon(new ImageIcon(v.getScaledInstance(height/2, -1, BufferedImage.SCALE_DEFAULT)));
		hsv.add(hue);
		hsv.add(sat);
		hsv.add(val);
		
		// Add the areas togheter
		this.add(sum);
		this.add(hsv);
		this.pack();
		this.setVisible(true);
	}
	
	/**
	 * Updates the calibration window with images in the sumhsv list
	 * @param sumhsv filtered images, in the order: summary, hue, saturation, value
	 */
	public void update(List<Mat> sumhsv){
		sum.setIcon(new ImageIcon(GuiUtils.Mat2BufferedImage(sumhsv.get(3))));
		BufferedImage h = GuiUtils.Mat2BufferedImage(sumhsv.get(0));
		BufferedImage s = GuiUtils.Mat2BufferedImage(sumhsv.get(1));
		BufferedImage v = GuiUtils.Mat2BufferedImage(sumhsv.get(2));
		
		hue.setIcon(new ImageIcon(h.getScaledInstance(height/2, width/2, BufferedImage.SCALE_DEFAULT)));
		sat.setIcon(new ImageIcon(s.getScaledInstance(height/2, width/2, BufferedImage.SCALE_DEFAULT)));
		val.setIcon(new ImageIcon(v.getScaledInstance(height/2, width/2, BufferedImage.SCALE_DEFAULT)));
	}
}
