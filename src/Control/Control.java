package Control;
import java.awt.Point;
import Model.*;

public class Control {
	
	private float P;
	private int width, height;
	
	public Control(int width, int height) {
		// Control parameter
		P = (float) 0.1;
		// Camera resolution scaling 
		this.width = width;
		this.height = height;
	}
	
	public PVector rotAngles(Point laserPoint, Point ballPoint) {
		
		// Determine the error in pixels
		Point error = new Point(ballPoint.x - laserPoint.x, ballPoint.y - laserPoint.y);
		
		// zRot is the rotation of the platform around the z-axis.
		// It will determine the left - right motion of the laser. 
		// So all the x-values are used here.
		// It is a delta angle that needs to be added to the current pose of the platform. 
		float zRot = P * error.x / width;
		
		// xRot is the rotation of the platform around the x-axis.
		// It will determine the up - down motion of the laser. 
		// So all the y-values are used here.
		// It is a delta angle that needs to be added to the current pose of the platform. 
		float xRot = -1.0f * P * error.y / height;
		
		// Send back to Pod
		return new PVector(xRot, 0, zRot);
	}
	
	public void setParam(float P) { 
		this.P = P;
	}
	
}