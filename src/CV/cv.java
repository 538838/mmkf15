package CV;

import java.util.ArrayList;
import java.util.List;

import org.opencv.core.*;
import org.opencv.imgproc.*;
import org.opencv.videoio.*;
import java.awt.Point;
import java.io.File;
import java.io.IOException;

public class CV {
	private VideoCapture camera;
	private List<Mat> HSV;
	private Mat raw;
	
	/**
	 * Creates a new computer vision-object 
	 * @param camera Camera to choose (starts from 0)
	 * @param width try to force CV-object to use specified width
	 * @param height try to force CV-object to use specified height
	 */
	public CV(int camera,int width,int height){
		this(camera);
		boolean W = this.camera.set(Videoio.CAP_PROP_FRAME_WIDTH, width);
		boolean H = this.camera.set(Videoio.CAP_PROP_FRAME_HEIGHT, height);
		if (!(W && H)){
			System.out.println("Failed to set camera size");
		}
	}
	
	/**
	 * Creates a new computer vision-object 
	 * @param camera Camera to choose (starts from 0)
	 */
	public CV(int camera){
		// Load CV library, which depends on architecture
		try {
			if (Integer.parseInt(System.getProperty("sun.arch.data.model")) == 64){ // 64bit system
				System.load(new File(".").getCanonicalPath() + "/opencv_java300_x64.dll");
			} else{ //Guessing its a 32bit system...
				System.load(new File(".").getCanonicalPath() + "/opencv_java300_x32.dll");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.camera = new VideoCapture(camera);
		HSV = new ArrayList<Mat>();
		raw = new Mat();
	}
	
	/**
	 * Closes everything that needs to be closed for the
	 * program to exit.
	 */
	public void close(){
		camera.release();
	}
	
	/**
	 * Grabs image from camera and stores it in the CV-object.
	 * @return raw BGR-image 
	 */
	public Mat fetchImage(){
		if(camera.isOpened()){
			Mat tmp = new Mat();
			//Grab new image from camera
			camera.read(raw);
			//Convert to HSV and split separate channels
			Imgproc.cvtColor(raw, tmp, Imgproc.COLOR_BGR2HSV);
			Core.split(tmp, HSV);
			return raw;
		}
		return null;
	}
	
	/**
	 * This uses simple thresholding in the HSV-channels to estimate a point. Of several pixels are found
	 * to match the thresholds the mean X- and Y- value is returned.
	 * 
	 * @param point Point to be changed.
	 * @param threshold int[] with min hue, max hue, min saturation, max saturation, min value, max value.
	 * @param copySource boolean. True if the HSV-array should be copied before use (and therefore not modified).
	 * @param visualizeResult boolean. True if Mat objects should be created and returned for visualization
	 * @return Array with filtered hue,saturation,value,raw image if visualizeResult = true. Null otherwise
	 */
	public List<Mat> findPositionSimple(Point point,int[] threshold,boolean copySource,boolean visualizeResult){
		List<Mat> tmp = new ArrayList<Mat>();		
		if(copySource){
			for (int i = 0; i < 3; i++) {
				tmp.add(HSV.get(i).clone());
			}
		} else {
			for (int i = 0; i < 3; i++) {
				tmp.add(HSV.get(i));
			}
		}
		
		//Simple threshold filtering, just assuming that int[] threshold size is correct
		if(threshold[0] != 0){
			Imgproc.threshold(tmp.get(0),tmp.get(0) ,threshold[0], 255, Imgproc.THRESH_TOZERO);
		}
		if(threshold[1] != 256){
			Imgproc.threshold(tmp.get(0),tmp.get(0) ,threshold[1], 255, Imgproc.THRESH_TOZERO_INV);
		}
		if(threshold[2] != 0){
			Imgproc.threshold(tmp.get(1),tmp.get(1) ,threshold[2], 255, Imgproc.THRESH_TOZERO);
		}
		if(threshold[3] != 256){
			Imgproc.threshold(tmp.get(1),tmp.get(1) ,threshold[3], 255, Imgproc.THRESH_TOZERO_INV);
		}
		if(threshold[4] != 0){
			Imgproc.threshold(tmp.get(2),tmp.get(2) ,threshold[4], 255, Imgproc.THRESH_TOZERO);
		}
  	  	if(threshold[5] != 256){
  	  		Imgproc.threshold(tmp.get(2),tmp.get(2) ,threshold[5], 255, Imgproc.THRESH_TOZERO_INV);
  	  	}
  	  	
  	  	//Calculate mean position of non-zero pixels. 
  	  	// Slowest part (with a factor of 10) of the computer vision, could be replaced by some openCV-function.
  	  	long sumR = 0;
  	  	long sumC = 0;
  	  	int num = 0;
  	  	for(int c = 0; c < tmp.get(0).cols();c++){
  	  		for(int r = 0; r < tmp.get(0).rows();r++){
  	  			if(tmp.get(0).get(r, c)[0] != 0.0 && tmp.get(1).get(r, c)[0] != 0.0 && tmp.get(2).get(r, c)[0] != 0.0){
  	  				sumR += r;
	  				sumC += c;
	  				num++;
  	  			}
  	  		}
  	  	}
  	  	
  	  	// If none is found return (-1,-1) else return the mean point
  	  	if(num == 0){
  	  		point.x = -1;
  	  		point.y = -1;
  	  	}else{
  	  		point.x =(int)(sumC/num);
  	  		point.y = (int)(sumR/num);
  	  	}
  	  	
  	  	// If visualizeResult = true, the filtered Hue, Saturation and value images together with the sum of them
  	  	// are returned in an list at Mat-objects. (Slows down calculation significantly)
  	  	if(visualizeResult){
  	  		Mat vis = new Mat(raw.rows(),raw.cols(),HSV.get(0).type());
  	  		for(int c = 0; c < raw.cols();c++){
  	  			for(int r = 0; r < raw.rows();r++){
  	  				if(tmp.get(0).get(r, c)[0] != 0.0 && tmp.get(1).get(r, c)[0] != 0.0 && tmp.get(2).get(r, c)[0] != 0.0){
  	  					vis.put(r, c, raw.get(r, c));
  	  				} else{
  	  					vis.put(r, c, new double[]{0.0,0.0,0.0});
  	  				}
  	  			}
  	  		}
  	  		List<Mat> tmpvis = new ArrayList<Mat>();
  	  		tmpvis.add(tmp.get(0));
  	  		tmpvis.add(tmp.get(1));
  	  		tmpvis.add(tmp.get(2));
  	  		tmpvis.add(vis);
  	  		return tmpvis;
  	  	}
  	  	// Else return null.
		return null;
	}	
}
