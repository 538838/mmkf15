import TrackerPod.*;

public class Main {

	public static void main(String[] args) {
		// Catch arguments
		String serialport = null;
		int camera = -1;
		String lastargument = "";
		for(int i = 0; i < args.length; i++){
			switch(lastargument){
			case "-c":
				camera = Integer.parseInt(args[i]);
				break;
			case "-s":
				serialport = args[i];
				break;
			}
			lastargument = args[i];
		}
		Pod p = new Pod(serialport,camera);
		p.start();
	}
}
